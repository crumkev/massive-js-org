import Site from './Site';
import Article from './Article';

export default function ({typo, title, description, content}) {
  return (
    <Site {...{title, description, lang: 'en'}}>
      <Article {...{typo, title, content}} />
    </Site>
  );
};
