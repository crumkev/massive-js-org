import {readdirSync} from 'fs';
import {basename} from 'path';

import Site from './Site';
import Article from './Article';

export default function ({typo, title, noHeading, content}) {
  const heading = noHeading ? '' : title;
  const apiListItems = readdirSync('./pages/api').map(filename => {
    const name = basename(filename, '.md');
    const href = `/api/${name}`;

    return (
      <li>
        <a href={href}>{name}</a>
      </li>
    );
  });

  return (
    <Site {...{title, lang: 'en'}}>
      <div id='docs-container'>
        <nav>
          <ul>
            <li><a href='/docs/connecting'>Connecting</a></li>
            <li><a href='/docs/framework-examples'>Framework Examples</a></li>
            <li><a href='/docs/criteria-objects'>Criteria Objects</a></li>
            <li><a href='/docs/options-objects'>Options Objects</a></li>
            <li><a href='/docs/queries'>Queries</a></li>
            <li><a href='/docs/persistence'>Persistence</a></li>
            <li><a href='/docs/functions-and-scripts'>Functions and Scripts</a></li>
            <li><a href='/docs/working-with-documents'>Working with Documents</a></li>
            <li><a href='/docs/resultset-decomposition'>Resultset Decomposition</a></li>
            <li><a href='/docs/database-structures'>Database Structures</a></li>
            <li><a href='/docs/tasks-and-transactions'>Tasks and Transactions</a></li>
            <li><a href='/docs/migrations-and-massive'>Migrations and Massive</a></li>
            <li><h3>API Docs</h3></li>
            <li>
              <ul>
                {apiListItems}
              </ul>
            </li>
          </ul>
        </nav>
        <Article {...{typo, title: heading, content}} />
      </div>
    </Site>
  );
};
