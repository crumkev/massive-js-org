export default function ({title, description, lang}, children) {
  const metaDescription = description ? (<meta name='description' content={description} />) : null;

  return (
    <html lang={lang}>
      <head>
        <title>{title} | MassiveJS</title>
        <meta charset='utf-8' />
        {metaDescription}
        <link rel='stylesheet' href='/css/site.css' />
        <link href='https://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' />
        <link href='https://fonts.googleapis.com/css?family=Montserrat:300' rel='stylesheet' />
        <link rel='shortcut icon' type='image/x-icon' href='/img/favicon.ico' />
      </head>
      <body>
        <header>
          <h1><a href='/'>MassiveJS</a></h1>
          <ul class='links'>
            <li><a href='/get-started'>Get Started</a></li>
            <li><a href='/docs/connecting'>Documentation</a></li>
            <li><a href='https://gitlab.com/dmfay/massive-js'>Project</a></li>
          </ul>
        </header>
        <div id='main' role='main'>
          {children}
        </div>
      </body>
    </html>
  )
};
