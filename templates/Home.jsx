import Site from './Site';

export default function ({typo, title, description, content}) {
  return (
    <Site {...{title, description, lang: 'en'}}>
      <article>
        <div>
          {typo(content)}
        </div>
      </article>
    </Site>
  );
};
