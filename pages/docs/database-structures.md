---
layout: Documentation
title: Database Structures
---

Massive provides visibility and functionality for certain database structures beyond the standard set of tables, (ordinary) views, and functions.

<!-- vim-markdown-toc GFM -->

* [Materialized Views](#materialized-views)
  * [Refreshing Materialized Views](#refreshing-materialized-views)
* [Enums](#enums)
* [Sequences](#sequences)
  * [lastValue](#lastvalue)
  * [nextValue](#nextvalue)
  * [reset](#reset)

<!-- vim-markdown-toc -->

## Materialized Views

Like standard views, [materialized views](https://www.postgresql.org/docs/current/static/rules-materializedviews.html) are essentially a name attached to a `SELECT` query. Unlike standard views, materialized views store their results instead of executing the underlying query each time they're invoked, sacrificing realtime accuracy for performance.

### Refreshing Materialized Views

Materialized views must be refreshed whenever you need to ensure the information in them is up to date.

Materialized views ordinarily block reads while refreshing. To avoid this, invoke the function passing `true` to specify a concurrent refresh.

`refresh` returns an empty query result.

```javascript
db.cached_statistics.refresh(true) // concurrently
  .then(() => {...});
```

## Enums

An enumeration is a data type which represents one of a fixed set of text values. They can make convenient replacements for lookup tables, so long as existing values are truly fixed (values may be added but not removed).

Enums are fully loaded on startup and can be accessed through the `db.enums` property:

```javascript
db.enums.myenum; // -> ['one', 'two', 'three'];
```

Note that if you add new values to the enum you must either separately append them to `db.enums` _or_ invoke `db.reload()` to stay up to date.

## Sequences

Sequences are the counters which back up `SERIAL` and `BIGSERIAL` columns. When used as table primary keys, sequences operate transparently and give their next values to rows as they're inserted. However, sequences can also be used independently, in which case it can be useful to know certain things about their state.

Massive only loads independent sequences and ignores those which back table primary keys.

### lastValue

`lastValue` returns the current value of the sequence counter. This value may not actually reside anywhere in the database! When a sequence is incremented, the value is reserved even if the transaction which caused the incrementation fails.

```javascript
db.mysequence.lastValue().then(val => {
  // val is a number
});
```

### nextValue

`nextValue` increments the sequence counter and returns the latest value.

```javascript
db.mysequence.nextValue().then(val => {
  // val is a number
});
```

### reset

`reset` starts the sequence over at 1 (if called without arguments) or a value of your choosing.

```javascript
db.mysequence.reset(123).then(() => {
  // reset does not return a value, but the next value
  // acquired from the sequence will be 123.
});
```
