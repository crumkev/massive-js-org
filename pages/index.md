---
layout: Home
title: Home
description: 'A data access toolkit and ORM replacement for Node.js and PostgreSQL'
---

[![npm](https://img.shields.io/npm/v/massive.svg?label=massive&style=popout)](https://npmjs.org/package/massive)
![node](https://img.shields.io/node/v/massive.svg)
[![Build Status](https://img.shields.io/gitlab/pipeline/dmfay/massive-js.svg)](https://gitlab.com/dmfay/massive-js/pipelines)
[![Coverage Status](https://coveralls.io/repos/gitlab/dmfay/massive-js/badge.svg)](https://coveralls.io/gitlab/dmfay/massive-js)
[![npm](https://img.shields.io/npm/dw/massive.svg)](https://npmjs.org/package/massive)

Massive is a data mapper for Node.js that goes all in on PostgreSQL and fully embraces the power and flexibility of the SQL language and relational metaphors. Providing minimal abstractions for the interfaces and tools you already use, its goal is to do just enough to make working with your data as easy and intuitive as possible, then get out of your way.

Massive is _not_ an object-relational mapper (ORM)! It doesn't use models, it doesn't track state, and it doesn't limit you to a single entity-based metaphor for accessing and persisting data. Massive connects to your database and introspects its schemas to build an API for the data model you already have: your tables, views, functions, and easily-modified SQL scripts.

Here are some of the highlights:

* **Dynamic query generation**: Massive's versatile query builder supports a wide variety of operators, all generated from a simple criteria object.
* **Low overhead**: An API built from your schema means no model classes to maintain, super-simple bulk operations, and direct access to your tables without any need to create or load entity instances beforehand.
* **Document storage**: PostgreSQL's JSONB storage type makes it possible to blend relational and document strategies. Massive offers a robust API to simplify working with documents: objects in, objects out, with document metadata managed for you.
* **Relational awareness**: Massive does not traverse relationships or build model graphs, but [deep inserts](/docs/persistence#deep-insert) can create related entities and junctions transactionally, and the [`decompose` option](/docs/resultset-decomposition) allows you to map the results of complex views and scripts to nested object trees.
* **Transactions**: Use `db.withTransaction` to execute a callback with full Massive API support in a transaction scope, getting a promise which fulfills if it commits or rejects if it rolls back.
* **Postgres everything**: Commitment to a single RDBMS lets us use it to its full potential. Massive supports array fields and operations, regular expression matching, foreign tables, materialized views, and more features found in PostgreSQL but not in other databases.

## Install

```bash
npm install massive --save
```

For next steps after that, see [Get Started](/get-started).

## Contributing

Visit the [project page](https://gitlab.com/dmfay/massive) to report issues or check out the code!

## Licensing

Massive is released under the [BSD 3-Clause license](https://gitlab.com/dmfay/massive-js/blob/master/LICENSE). You may use it free of charge, including in commercial software products you release under your own warranty. You may not hold the project or its contributors liable for any reason or claim its contributors endorse whatever you're doing with it. If you redistribute Massive you must include the copyright and license. This is automatic assuming you're using a package manager like npm or yarn.

MassiveJS.org content is covered by the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0) license](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode). You may copy and modify it to your heart's content, but if you want to share or distribute your modifications you can't make money off it, and your derivative work must be released under the same terms.
